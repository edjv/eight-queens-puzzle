const NONE = 0
const QUEEN = 1

class Tile {
    constructor(init) {
        this.occupant = init || NONE
    }

    put(occupant) {
        this.occupant = occupant
    }

    removeOccupant() {
        this.occupant = NONE
    }

    stringify() {
        return this.occupant === QUEEN ? 'Q' : ' ';
    }
}

class Board {
    constructor(size) {
        this.size = size
        this.rows = Array(isNaN(size)? 8 : size).fill()
            .map(row => Array(isNaN(size)? 8 : size).fill()
                .map(() => new Tile())
            )
        this.numSolutions = 0
    }

    putNextQueen(rowNum) {
        const thisBoard = this
        const thisRow = thisBoard.rows[rowNum]
        thisRow.forEach((tile, colNum) => {
            if (this.isSafeToOccupy(rowNum, colNum)) {
                tile.put(QUEEN)
                if (rowNum < thisBoard.size - 1) {
                    thisBoard.putNextQueen(rowNum + 1)
                } else if (rowNum === thisBoard.size - 1) {
                    thisBoard.recordSolution()
                }
                tile.removeOccupant()
            }
        }, thisBoard)
    }

    isSafeToOccupy(rowNum, colNum) {
        return [].concat(
            this.getRowAt(rowNum),
            this.getColAt(colNum),
            this.getDiagsAt(rowNum, colNum)
        )
        .every(tile => tile.occupant === NONE)
    }

    getRowAt(rowNum) {
        return this.rows[rowNum]
    }

    getColAt(colNum) {
        return this.rows
            .map(row => row[colNum])
    }

    getDiagsAt(rowNum, colNum) {
        return this.rows
            .map((row, rowIndex) => [
                row[colNum + (rowIndex - rowNum)],
                row[colNum - (rowIndex - rowNum)]
            ])
            .reduce((acc, els) => acc.concat(els), [])
            .filter(Boolean)
    }

    recordSolution() {
        this.numSolutions++
        console.log("Solution #" + this.numSolutions)
        this.draw()
    }

    draw() {
        this.rows.forEach((row) => {
            console.log(row
                .map(tile => tile.stringify())
                .join(',')
            )
        })
    }
}

function solve(size) {
    const b = new Board(size || 8)
    b.putNextQueen(0)
    return b.numSolutions
}

module.exports = {
    solve: solve
}
